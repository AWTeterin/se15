package ru.teterin.tm.exception;

import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.constant.Constant;

public final class ObjectNotFoundException extends RuntimeException {

    public ObjectNotFoundException() {
        super(Constant.OBJECT_NOT_FOUND);
    }

    public ObjectNotFoundException(@Nullable final String message) {
        super(message);
    }

}
