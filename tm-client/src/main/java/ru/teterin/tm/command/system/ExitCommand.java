package ru.teterin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class ExitCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "exit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Close application.";
    }

    @Override
    public void execute() throws Exception_Exception {
        @Nullable final String session = stateService.getSession();
        if (session != null) {
            sessionEndpoint.closeSession(session);
        }
        System.exit(Constant.WITHOUT_ERRORS);
    }

    @Override
    public boolean secure() {
        return false;
    }

}
