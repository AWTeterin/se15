package ru.teterin.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.api.endpoint.Role;
import ru.teterin.tm.api.endpoint.User;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.util.PasswordHashUtil;

import java.util.UUID;

public final class UserCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "user-reg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Registration new user.";
    }

    @Override
    public void execute() throws Exception_Exception {
        terminalService.print(Constant.USER_REGISTRATION);
        terminalService.print(Constant.ENTER_LOGIN);
        @Nullable final String login = terminalService.readString();
        userEndpoint = serviceLocator.getUserEndpoint();
        final boolean loginIsBusy = !userEndpoint.loginIsFree(login);
        if (loginIsBusy) {
            terminalService.print(Constant.LOGIN_BUSY);
            return;
        }
        @NotNull final User user = new User();
        user.setLogin(login);
        terminalService.print(Constant.ENTER_PASSWORD);
        @Nullable final String password = terminalService.readString();
        @Nullable final String hashPassword = PasswordHashUtil.md5(password);
        user.setPassword(hashPassword);
        user.setRole(Role.USER);
        @NotNull final String userId = UUID.randomUUID().toString();
        user.setId(userId);
        userEndpoint.persistUser(user);
        sessionEndpoint = serviceLocator.getSessionEndpoint();
        @Nullable String session = stateService.getSession();
        if (session != null) {
            sessionEndpoint.closeSession(session);
        }
        session = sessionEndpoint.openSession(login, password);
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_LOGIN_OR_PASSWORD);
        }
        stateService.setSession(session);
        stateService.setLogin(login);
        @NotNull final String helloMessage = Constant.HELLO + login;
        terminalService.print(helloMessage);
    }

    @Override
    public boolean secure() {
        return false;
    }

}
