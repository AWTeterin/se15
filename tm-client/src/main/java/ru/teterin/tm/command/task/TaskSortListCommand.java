package ru.teterin.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.api.endpoint.Task;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

import java.util.Collection;

public final class TaskSortListCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-sort";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Find all tasks and sort by field and type sort. Show result.";
    }

    @Override
    public void execute() throws Exception_Exception {
        terminalService.print(Constant.TASK_SORT);
        @Nullable final String session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        terminalService.print(Constant.ENTER_SORT_FIELD);
        @Nullable final String sortOptions = terminalService.readString();
        taskEndpoint = serviceLocator.getTaskEndpoint();
        @NotNull final Collection<Task> tasks = taskEndpoint.sortAllTask(session, sortOptions);
        terminalService.printCollection(tasks);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
