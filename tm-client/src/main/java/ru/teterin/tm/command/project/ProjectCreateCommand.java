package ru.teterin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Exception_Exception;
import ru.teterin.tm.api.endpoint.Project;
import ru.teterin.tm.command.AbstractCommand;
import ru.teterin.tm.constant.Constant;

public final class ProjectCreateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new Project.";
    }

    @Override
    public void execute() throws Exception_Exception {
        terminalService.print(Constant.PROJECT_CREATE);
        @Nullable final String session = stateService.getSession();
        if (session == null) {
            throw new IllegalArgumentException(Constant.INCORRECT_COMMAND);
        }
        @NotNull final Project project = terminalService.readProject();
        projectEndpoint = serviceLocator.getProjectEndpoint();
        projectEndpoint.persistProject(session, project);
        terminalService.print(Constant.CORRECT_EXECUTION);
    }

    @Override
    public boolean secure() {
        return true;
    }

}
