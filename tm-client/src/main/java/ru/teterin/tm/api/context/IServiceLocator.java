package ru.teterin.tm.api.context;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.endpoint.IProjectEndpoint;
import ru.teterin.tm.api.endpoint.ISessionEndpoint;
import ru.teterin.tm.api.endpoint.ITaskEndpoint;
import ru.teterin.tm.api.endpoint.IUserEndpoint;

public interface IServiceLocator {

    @NotNull
    public ISessionEndpoint getSessionEndpoint();

    @NotNull
    public IUserEndpoint getUserEndpoint();

    @NotNull
    public IProjectEndpoint getProjectEndpoint();

    @NotNull
    public ITaskEndpoint getTaskEndpoint();

}
