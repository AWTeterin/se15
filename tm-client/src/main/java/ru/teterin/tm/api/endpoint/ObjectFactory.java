package ru.teterin.tm.api.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the ru.teterin.tm.api.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "Exception");
    private final static QName _FindAllTask_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "findAllTask");
    private final static QName _FindAllTaskResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "findAllTaskResponse");
    private final static QName _FindOneTask_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "findOneTask");
    private final static QName _FindOneTaskResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "findOneTaskResponse");
    private final static QName _LinkTask_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "linkTask");
    private final static QName _LinkTaskResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "linkTaskResponse");
    private final static QName _MergeTask_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "mergeTask");
    private final static QName _MergeTaskResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "mergeTaskResponse");
    private final static QName _PersistTask_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "persistTask");
    private final static QName _PersistTaskResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "persistTaskResponse");
    private final static QName _RemoveAllTasks_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "removeAllTasks");
    private final static QName _RemoveAllTasksResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "removeAllTasksResponse");
    private final static QName _RemoveTask_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "removeTask");
    private final static QName _RemoveTaskResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "removeTaskResponse");
    private final static QName _SearchTaskByString_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "searchTaskByString");
    private final static QName _SearchTaskByStringResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "searchTaskByStringResponse");
    private final static QName _SortAllTask_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "sortAllTask");
    private final static QName _SortAllTaskResponse_QNAME = new QName("http://endpoint.api.tm.teterin.ru/", "sortAllTaskResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.teterin.tm.api.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link FindAllTask }
     */
    public FindAllTask createFindAllTask() {
        return new FindAllTask();
    }

    /**
     * Create an instance of {@link FindAllTaskResponse }
     */
    public FindAllTaskResponse createFindAllTaskResponse() {
        return new FindAllTaskResponse();
    }

    /**
     * Create an instance of {@link FindOneTask }
     */
    public FindOneTask createFindOneTask() {
        return new FindOneTask();
    }

    /**
     * Create an instance of {@link FindOneTaskResponse }
     */
    public FindOneTaskResponse createFindOneTaskResponse() {
        return new FindOneTaskResponse();
    }

    /**
     * Create an instance of {@link LinkTask }
     */
    public LinkTask createLinkTask() {
        return new LinkTask();
    }

    /**
     * Create an instance of {@link LinkTaskResponse }
     */
    public LinkTaskResponse createLinkTaskResponse() {
        return new LinkTaskResponse();
    }

    /**
     * Create an instance of {@link MergeTask }
     */
    public MergeTask createMergeTask() {
        return new MergeTask();
    }

    /**
     * Create an instance of {@link MergeTaskResponse }
     */
    public MergeTaskResponse createMergeTaskResponse() {
        return new MergeTaskResponse();
    }

    /**
     * Create an instance of {@link PersistTask }
     */
    public PersistTask createPersistTask() {
        return new PersistTask();
    }

    /**
     * Create an instance of {@link PersistTaskResponse }
     */
    public PersistTaskResponse createPersistTaskResponse() {
        return new PersistTaskResponse();
    }

    /**
     * Create an instance of {@link RemoveAllTasks }
     */
    public RemoveAllTasks createRemoveAllTasks() {
        return new RemoveAllTasks();
    }

    /**
     * Create an instance of {@link RemoveAllTasksResponse }
     */
    public RemoveAllTasksResponse createRemoveAllTasksResponse() {
        return new RemoveAllTasksResponse();
    }

    /**
     * Create an instance of {@link RemoveTask }
     */
    public RemoveTask createRemoveTask() {
        return new RemoveTask();
    }

    /**
     * Create an instance of {@link RemoveTaskResponse }
     */
    public RemoveTaskResponse createRemoveTaskResponse() {
        return new RemoveTaskResponse();
    }

    /**
     * Create an instance of {@link SearchTaskByString }
     */
    public SearchTaskByString createSearchTaskByString() {
        return new SearchTaskByString();
    }

    /**
     * Create an instance of {@link SearchTaskByStringResponse }
     */
    public SearchTaskByStringResponse createSearchTaskByStringResponse() {
        return new SearchTaskByStringResponse();
    }

    /**
     * Create an instance of {@link SortAllTask }
     */
    public SortAllTask createSortAllTask() {
        return new SortAllTask();
    }

    /**
     * Create an instance of {@link SortAllTaskResponse }
     */
    public SortAllTaskResponse createSortAllTaskResponse() {
        return new SortAllTaskResponse();
    }

    /**
     * Create an instance of {@link Task }
     */
    public Task createTask() {
        return new Task();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTask }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "findAllTask")
    public JAXBElement<FindAllTask> createFindAllTask(FindAllTask value) {
        return new JAXBElement<FindAllTask>(_FindAllTask_QNAME, FindAllTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllTaskResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "findAllTaskResponse")
    public JAXBElement<FindAllTaskResponse> createFindAllTaskResponse(FindAllTaskResponse value) {
        return new JAXBElement<FindAllTaskResponse>(_FindAllTaskResponse_QNAME, FindAllTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTask }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "findOneTask")
    public JAXBElement<FindOneTask> createFindOneTask(FindOneTask value) {
        return new JAXBElement<FindOneTask>(_FindOneTask_QNAME, FindOneTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindOneTaskResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "findOneTaskResponse")
    public JAXBElement<FindOneTaskResponse> createFindOneTaskResponse(FindOneTaskResponse value) {
        return new JAXBElement<FindOneTaskResponse>(_FindOneTaskResponse_QNAME, FindOneTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkTask }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "linkTask")
    public JAXBElement<LinkTask> createLinkTask(LinkTask value) {
        return new JAXBElement<LinkTask>(_LinkTask_QNAME, LinkTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LinkTaskResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "linkTaskResponse")
    public JAXBElement<LinkTaskResponse> createLinkTaskResponse(LinkTaskResponse value) {
        return new JAXBElement<LinkTaskResponse>(_LinkTaskResponse_QNAME, LinkTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeTask }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "mergeTask")
    public JAXBElement<MergeTask> createMergeTask(MergeTask value) {
        return new JAXBElement<MergeTask>(_MergeTask_QNAME, MergeTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MergeTaskResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "mergeTaskResponse")
    public JAXBElement<MergeTaskResponse> createMergeTaskResponse(MergeTaskResponse value) {
        return new JAXBElement<MergeTaskResponse>(_MergeTaskResponse_QNAME, MergeTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistTask }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "persistTask")
    public JAXBElement<PersistTask> createPersistTask(PersistTask value) {
        return new JAXBElement<PersistTask>(_PersistTask_QNAME, PersistTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersistTaskResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "persistTaskResponse")
    public JAXBElement<PersistTaskResponse> createPersistTaskResponse(PersistTaskResponse value) {
        return new JAXBElement<PersistTaskResponse>(_PersistTaskResponse_QNAME, PersistTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllTasks }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "removeAllTasks")
    public JAXBElement<RemoveAllTasks> createRemoveAllTasks(RemoveAllTasks value) {
        return new JAXBElement<RemoveAllTasks>(_RemoveAllTasks_QNAME, RemoveAllTasks.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllTasksResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "removeAllTasksResponse")
    public JAXBElement<RemoveAllTasksResponse> createRemoveAllTasksResponse(RemoveAllTasksResponse value) {
        return new JAXBElement<RemoveAllTasksResponse>(_RemoveAllTasksResponse_QNAME, RemoveAllTasksResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTask }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "removeTask")
    public JAXBElement<RemoveTask> createRemoveTask(RemoveTask value) {
        return new JAXBElement<RemoveTask>(_RemoveTask_QNAME, RemoveTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveTaskResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "removeTaskResponse")
    public JAXBElement<RemoveTaskResponse> createRemoveTaskResponse(RemoveTaskResponse value) {
        return new JAXBElement<RemoveTaskResponse>(_RemoveTaskResponse_QNAME, RemoveTaskResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchTaskByString }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "searchTaskByString")
    public JAXBElement<SearchTaskByString> createSearchTaskByString(SearchTaskByString value) {
        return new JAXBElement<SearchTaskByString>(_SearchTaskByString_QNAME, SearchTaskByString.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SearchTaskByStringResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "searchTaskByStringResponse")
    public JAXBElement<SearchTaskByStringResponse> createSearchTaskByStringResponse(SearchTaskByStringResponse value) {
        return new JAXBElement<SearchTaskByStringResponse>(_SearchTaskByStringResponse_QNAME, SearchTaskByStringResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SortAllTask }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "sortAllTask")
    public JAXBElement<SortAllTask> createSortAllTask(SortAllTask value) {
        return new JAXBElement<SortAllTask>(_SortAllTask_QNAME, SortAllTask.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SortAllTaskResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.api.tm.teterin.ru/", name = "sortAllTaskResponse")
    public JAXBElement<SortAllTaskResponse> createSortAllTaskResponse(SortAllTaskResponse value) {
        return new JAXBElement<SortAllTaskResponse>(_SortAllTaskResponse_QNAME, SortAllTaskResponse.class, null, value);
    }

}
