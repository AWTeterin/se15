package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.endpoint.Project;
import ru.teterin.tm.api.endpoint.Task;

import java.util.Collection;

public interface ITerminalService {

    @Nullable
    public String readString();

    @NotNull
    public Project readProject();

    @NotNull
    public Task readTask();

    public void print(
        @Nullable final Object object
    );

    public void printCollection(
        @NotNull final Collection collection
    );

}
