package ru.teterin.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import ru.teterin.tm.api.endpoint.User;

public final class UserEndpointTest extends AbstractTest {

    @Test
    public void findOneUser() throws Exception {
        userEndpoint.persistUser(user);
        @Nullable final User userDb = userEndpoint.findOneUser(adminToken, userId);
        Assert.assertNotNull(userDb);
        Assert.assertEquals(user, userDb);
        userEndpoint.removeUser(adminToken, userId);
    }

    @Test
    public void persistUser() throws Exception {
        userEndpoint.persistUser(user);
        @Nullable final User userDb = userEndpoint.findOneUser(adminToken, userId);
        Assert.assertNotNull(userDb);
        Assert.assertEquals(user, userDb);
        userEndpoint.removeUser(adminToken, userId);
    }

    @Test
    public void mergeUser() throws Exception {
        userEndpoint.persistUser(user);
        @NotNull final String testToken = sessionEndpoint.openSession(user.getLogin(), user.getPassword());
        @NotNull final String newTestName = "newTest";
        userEndpoint.setLogin(testToken, newTestName);
        @Nullable final User userDb = userEndpoint.findOneUser(adminToken, userId);
        Assert.assertNotNull(userDb);
        Assert.assertEquals(newTestName, userDb.getLogin());
        userEndpoint.removeUser(adminToken, userId);
    }

    @Test
    public void removeUser() throws Exception {
        userEndpoint.persistUser(user);
        @Nullable final User userDb = userEndpoint.findOneUser(adminToken, userId);
        Assert.assertNotNull(userDb);
        userEndpoint.removeUser(adminToken, userDb.getId());
        try {
            @Nullable final User result = userEndpoint.findOneUser(adminToken, userId);
            Assert.fail("Excepted ObjectNotFoundException");
        } catch (Exception ignored) {
        }
    }

    @Test
    public void loginIsFree() throws Exception {
        userEndpoint.persistUser(user);
        final boolean loginIsFree = userEndpoint.loginIsFree(user.getLogin());
        Assert.assertFalse(loginIsFree);
        userEndpoint.removeUser(adminToken, userId);
    }

}
