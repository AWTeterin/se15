# Task Manager
https://gitlab.com/AWTeterin/se15

## Software
    -JRE
    -Java 1.8
    -Maven 4.0.0
    
## Technology Stack
    -Maven
    -Git
    -Java SE
    -Hibernate

## Developer
   Teterin Alexei    
   email: [teterin2012@rambler.ru](teterin2012@rambler.ru)

## Commands for building the app

    mvn clean install
## Commands for run the app

### Server run
    java -jar tm-server/target/release/bin/tm-server.jar

### Client run
    java -jar tm-client/target/release/bin/tm-client.jar