package ru.teterin.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class FieldConstant {

    @NotNull
    public static final String ID = "id";

    @NotNull
    public static final String LOGIN = "login";

    @NotNull
    public static final String PASSWORD = "password";

    @NotNull
    public static final String ROLE = "role";

    @NotNull
    public static final String USER_ID = "userId";

    @NotNull
    public static final String SIGNATURE = "signature";

    @NotNull
    public static final String TIMESTAMP = "timestamp";

    @NotNull
    public static final String DATE_CREATE = "dateCreate";

    @NotNull
    public static final String NAME = "name";

    @NotNull
    public static final String DESCRIPTION = "description";

    @NotNull
    public static final String DATE_START = "dateStart";

    @NotNull
    public static final String DATE_END = "dateEnd";

    @NotNull
    public static final String STATUS = "status";

    @NotNull
    public static final String PROJECT_ID = "projectId";

}
