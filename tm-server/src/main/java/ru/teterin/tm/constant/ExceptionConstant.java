package ru.teterin.tm.constant;

import org.jetbrains.annotations.NotNull;

public final class ExceptionConstant {

    @NotNull
    public static final String INCORRECT_PASSWORD = "INCORRECT PASSWORD! TRY A DIFFERENT PASSWORD!";

    @NotNull
    public static final String INCORRECT_ID = "INCORRECT FORMAT ID! TRY THIS FORMAT: 00000000-0000-0000-0000-000000000000";

    @NotNull
    public static final String INCORRECT_DATE = "INCORRECT FORMAT DATE! TRY THIS FORMAT: 01.01.2020";

    @NotNull
    public static final String INCORRECT_DATA_FILE = "INCORRECT FILE! UNABLE TO READ DATA FROM THE FILE!";

    @NotNull
    public static final String OBJECT_NOT_FOUND = "OBJECT NOT FOUND!";

    @NotNull
    public static final String FILE_NOT_FOUND = "FILE NOT FOUND!";

    @NotNull
    public static final String NO_PROJECT = "YOU CAN'T CREATE AN ISSUE WITHOUT A PROJECT! FIRST, CREATE A PROJECT!";

    @NotNull
    public static final String NO_TASK = "THE TASK CANNOT BE EMPTY!";

    @NotNull
    public static final String NO_DOMAIN = "THE DOMAIN CANNOT BE EMPTY!";

    @NotNull
    public static final String NO_SESSION = "THE SESSION CANNOT BE EMPTY!";

    @NotNull
    public static final String EMPTY_ID = "ID CAN'T BE EMPTY!";

    @NotNull
    public static final String EMPTY_OBJECT = "OBJECT OR ITS FIELDS CANNOT BE EMPTY!";

    @NotNull
    public static final String EMPTY_LOGIN = "LOGIN CANT BE EMPTY!";

    @NotNull
    public static final String EMPTY_PASSWORD = "PASSWORD CANT BE EMPTY!";

    @NotNull
    public static final String ERROR_OBJECT_ACCESS = "OBJECT CANNOT BE ACCESSED!";

    @NotNull
    public static final String EXCEPTION_REMOVE = "CANT REMOVE OBJECT(S)!";

    @NotNull
    public static final String EXCEPTION_UPDATE = "CANT UPDATE OBJECT!";

}
