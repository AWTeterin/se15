package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.ISessionRepository;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.SessionPOJO;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private final EntityManager entityManager;

    public SessionRepository(
        @NotNull final EntityManager entityManager
    ) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public SessionPOJO findOne(
        @NotNull final String id
    ) {
        return entityManager.createQuery("SELECT s FROM SessionPOJO s WHERE s.id = :id", SessionPOJO.class)
            .setParameter("id", id).getSingleResult();
    }

    @NotNull
    @Override
    public List<SessionPOJO> findAll() {
        return entityManager.createQuery("SELECT s FROM SessionPOJO s", SessionPOJO.class)
            .getResultList();
    }

    @Override
    public void persist(
        @NotNull final SessionPOJO session
    ) {
        entityManager.persist(session);
    }

    @Override
    public void remove(
        @NotNull final String id
    ) throws Exception {
        @Nullable final SessionPOJO session = findOne(id);
        if (session == null) {
            entityManager.close();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        }
        entityManager.remove(session);
    }

    @Override
    public void removeAll() {
        @NotNull final List<SessionPOJO> sessions = findAll();
        for (@NotNull final SessionPOJO session : sessions) {
            entityManager.remove(session);
        }
    }

    @Override
    public boolean contains(
        @NotNull final String id
    ) {
        return !(entityManager.createQuery("SELECT s FROM SessionPOJO s WHERE s.id = :id")
            .setParameter("id", id).getResultList().isEmpty());
    }

}
