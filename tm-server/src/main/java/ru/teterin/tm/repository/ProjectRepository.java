package ru.teterin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.ProjectPOJO;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    public ProjectRepository(
        @NotNull final EntityManager entityManager
    ) {
        this.entityManager = entityManager;
    }

    @Nullable
    @Override
    public ProjectPOJO findOne(
        @NotNull final String userId,
        @NotNull final String id
    ) {
        @NotNull final List<ProjectPOJO> projects = entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.id = :id AND p.user.id = :userId", ProjectPOJO.class)
            .setParameter("id", id).setParameter("userId", userId).getResultList();
        if (projects.isEmpty()) {
            return null;
        }
        return projects.get(0);
    }

    @NotNull
    @Override
    public List<ProjectPOJO> findAll() {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p", ProjectPOJO.class).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectPOJO> findAll(
        @NotNull final String userId
    ) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.user.id = :userId", ProjectPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

    @Override
    public void persist(
        @NotNull final ProjectPOJO project
    ) {
        entityManager.persist(project);
    }

    @Override
    public void merge(
        @NotNull final ProjectPOJO project
    ) {
        entityManager.merge(project);
    }

    @Override
    public void remove(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception {
        @Nullable final ProjectPOJO project = findOne(userId, id);
        if (project == null) {
            entityManager.close();
            throw new Exception(ExceptionConstant.EXCEPTION_REMOVE);
        }
        entityManager.remove(project);
    }

    @Override
    public void removeAll() {
        @NotNull final List<ProjectPOJO> projects = findAll();
        for (@NotNull final ProjectPOJO project : projects) {
            entityManager.remove(project);
        }
    }

    @Override
    public void removeAll(
        @NotNull final String userId
    ) {
        @NotNull final List<ProjectPOJO> projects = findAll(userId);
        for (@NotNull final ProjectPOJO project : projects) {
            entityManager.remove(project);
        }
    }

    @NotNull
    @Override
    public List<ProjectPOJO> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    ) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.user.id = :userId " +
            "AND (p.name LIKE CONCAT('%', :searchString,'%') OR p.description LIKE CONCAT('%', :searchString,'%'))", ProjectPOJO.class)
            .setParameter("userId", userId).setParameter("searchString", searchString).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectPOJO> sortByDateCreate(
        @NotNull final String userId,
        @NotNull final String sortType
    ) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.user.id = :userId ORDER BY dateCreate", ProjectPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectPOJO> sortByDateStart(
        @NotNull final String userId,
        @NotNull final String sortType
    ) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.user.id  = :userId ORDER BY dateStart", ProjectPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectPOJO> sortByDateEnd(
        @NotNull final String userId,
        @NotNull final String sortType
    ) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.user.id = :userId ORDER BY dateEnd", ProjectPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

    @NotNull
    @Override
    public List<ProjectPOJO> sortByStatus(
        @NotNull final String userId,
        @NotNull final String sortType
    ) {
        return entityManager.createQuery("SELECT p FROM ProjectPOJO p WHERE p.user.id = :userId ORDER BY status", ProjectPOJO.class)
            .setParameter("userId", userId).getResultList();
    }

}
