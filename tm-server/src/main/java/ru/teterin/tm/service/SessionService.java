package ru.teterin.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.ISessionRepository;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IPropertyService;
import ru.teterin.tm.api.service.ISessionService;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.SessionPOJO;
import ru.teterin.tm.entity.UserPOJO;
import ru.teterin.tm.enumerated.Role;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.repository.SessionRepository;
import ru.teterin.tm.repository.UserRepository;
import ru.teterin.tm.util.AES;
import ru.teterin.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public final class SessionService extends AbstractService implements ISessionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public SessionService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.propertyService = serviceLocator.getPropertyService();
        this.entityManagerFactory = serviceLocator.getEntityManagerFactory();
    }

    @NotNull
    private SessionPOJO open(
        @Nullable final String login,
        @Nullable final String password
    ) throws Exception {
        final boolean noLogin = login == null || login.isEmpty();
        if (noLogin) {
            throw new Exception(ExceptionConstant.EMPTY_LOGIN);
        }
        final boolean noPassword = password == null || password.isEmpty();
        if (noPassword) {
            throw new Exception(ExceptionConstant.EMPTY_PASSWORD);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final UserPOJO userPOJO = userRepository.findByLogin(login);
        if (userPOJO == null) {
            throw new Exception(ExceptionConstant.OBJECT_NOT_FOUND);
        }
        @NotNull final String userPassword = userPOJO.getPassword();
        final boolean incorrectPassword = !password.equals(userPassword);
        if (incorrectPassword) {
            throw new Exception(ExceptionConstant.INCORRECT_PASSWORD);
        }
        @NotNull SessionPOJO session = new SessionPOJO();
        session.setUser(userPOJO);
        @Nullable final Role userRole = userPOJO.getRole();
        session.setRole(userRole);
        sign(session);
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        sessionRepository.persist(session);
        entityManager.getTransaction().commit();
        entityManager.close();
        return session;
    }

    @Override
    public void validate(
        @Nullable final String session
    ) throws Exception {
        if (session == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final SessionPOJO result = decryptSession(session);
        if (result == null) {
            throw new AccessForbiddenException();
        }
        @NotNull final UserPOJO userPOJO = result.getUser();
        if (userPOJO == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final String userId = userPOJO.getId();
        final boolean noUserId = userId == null || userId.isEmpty();
        if (noUserId) {
            throw new AccessForbiddenException();
        }
        @Nullable final String signature = result.getSignature();
        final boolean noSignature = signature == null || signature.isEmpty();
        if (noSignature) {
            throw new AccessForbiddenException();
        }
        @NotNull final SessionPOJO temp = result.clone();
        if (temp == null) {
            throw new AccessForbiddenException();
        }
        @NotNull final String signatureSource = result.getSignature();
        sign(temp);
        @Nullable final String signatureTarget = temp.getSignature();
        final boolean incorrectSignature = !signatureSource.equals(signatureTarget);
        if (incorrectSignature) {
            throw new AccessForbiddenException();
        }
        @NotNull final String sessionId = result.getId();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        final boolean incorrectId = !sessionRepository.contains(sessionId);
        if (incorrectId) {
            throw new AccessForbiddenException();
        }
        entityManager.close();
    }

    @Override
    public void validateWithRole(
        @Nullable final String session,
        @Nullable final Role role
    ) throws Exception {
        validate(session);
        @NotNull final SessionPOJO temp = decryptSession(session);
        @NotNull final Role userRole = temp.getRole();
        final boolean accessDenied = !userRole.equals(role);
        if (accessDenied) {
            throw new AccessForbiddenException();
        }
    }

    @Override
    public void close(
        @Nullable final String session
    ) throws Exception {
        validate(session);
        @Nullable final SessionPOJO temp = decryptSession(session);
        @NotNull final String sessionId = temp.getId();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        sessionRepository.remove(sessionId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    private SessionPOJO sign(
        @Nullable final SessionPOJO sessionPOJO
    ) {
        if (sessionPOJO == null) {
            return null;
        }
        sessionPOJO.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSalt();
        @NotNull final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(sessionPOJO, salt, cycle);
        sessionPOJO.setSignature(signature);
        return sessionPOJO;
    }

    @Nullable
    @Override
    public String getToken(
        @Nullable final String login,
        @Nullable final String password
    ) throws Exception {
        @NotNull final SessionPOJO session = open(login, password);
        @Nullable final String encryptedSession = encryptSession(session);
        return encryptedSession;
    }

    @Nullable
    private String encryptSession(
        @Nullable final SessionPOJO session
    ) throws Exception {
        @NotNull final String key = propertyService.getSecretKey();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        return AES.encrypt(json, key);
    }

    @Nullable
    private SessionPOJO decryptSession(
        @Nullable final String session
    ) throws Exception {
        @NotNull final String key = propertyService.getSecretKey();
        if (session == null) {
            return null;
        }
        @NotNull final String json = AES.decrypt(session, key);
        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @NotNull final SessionPOJO result = mapper.readValue(json, SessionPOJO.class);
        return result;
    }

    @NotNull
    @Override
    public String getUserId(
        @Nullable final String session
    ) throws Exception {
        if (session == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final SessionPOJO temp = decryptSession(session);
        if (temp == null) {
            throw new AccessForbiddenException();
        }
        @Nullable final String id = temp.getId();
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ISessionRepository sessionRepository = new SessionRepository(entityManager);
        @Nullable final SessionPOJO sessionBd = sessionRepository.findOne(id);
        entityManager.close();
        if (sessionBd == null) {
            throw new AccessForbiddenException();
        }
        @NotNull final String userId = sessionBd.getUser().getId();
        if (userId == null) {
            throw new AccessForbiddenException();
        }
        return userId;
    }

}
