package ru.teterin.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.IProjectService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.ProjectPOJO;
import ru.teterin.tm.entity.TaskPOJO;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public final class ProjectService extends AbstractService implements IProjectService {

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public ProjectService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.entityManagerFactory = serviceLocator.getEntityManagerFactory();
    }

    @NotNull
    @Override
    public ProjectPOJO findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final ProjectPOJO project = projectRepository.findOne(userId, id);
        entityManager.close();
        if (project == null) {
            throw new Exception(ExceptionConstant.OBJECT_NOT_FOUND);
        }
        return project;
    }

    @NotNull
    @Override
    public List<ProjectPOJO> findAll() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final List<ProjectPOJO> projects = projectRepository.findAll();
        entityManager.close();
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectPOJO> findAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final List<ProjectPOJO> projects = projectRepository.findAll(userId);
        entityManager.close();
        return projects;
    }

    @Override
    public void persist(
        @Nullable final ProjectPOJO project
    ) throws Exception {
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.persist(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(
        @Nullable final String userId,
        @Nullable final ProjectPOJO project
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (project == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = project.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String projectUserId = project.getUser().getId();
        if (!userId.equals(projectUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.merge(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.remove(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        projectRepository.removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @NotNull
    @Override
    public List<TaskPOJO> findAllTaskByProjectId(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        try{
            findOne(userId, id);
        } catch (Exception e) {
            return Collections.EMPTY_LIST;
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskPOJO> tasks = taskRepository.findAllByProjectId(userId, id);
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<ProjectPOJO> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (sortOption == null) {
            sortOption = Constant.CREATE_ASC_SORT;
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectPOJO> projects;
        switch (sortOption) {
            default:
                projects = projectRepository.sortByDateCreate(userId, Constant.ASC);
                break;
            case Constant.CREATE_DESC_SORT:
                projects = projectRepository.sortByDateCreate(userId, Constant.DESC);
                break;
            case Constant.START_DATE_ASC_SORT:
                projects = projectRepository.sortByDateStart(userId, Constant.ASC);
                break;
            case Constant.START_DATE_DESC_SORT:
                projects = projectRepository.sortByDateStart(userId, Constant.DESC);
                break;
            case Constant.END_DATE_ASC_SORT:
                projects = projectRepository.sortByDateEnd(userId, Constant.ASC);
                break;
            case Constant.END_DATE_DESC_SORT:
                projects = projectRepository.sortByDateEnd(userId, Constant.DESC);
                break;
            case Constant.STATUS_ASC_SORT:
                projects = projectRepository.sortByStatus(userId, Constant.ASC);
                break;
            case Constant.STATUS_DESC_SORT:
                projects = projectRepository.sortByStatus(userId, Constant.DESC);
                break;
        }
        entityManager.close();
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectPOJO> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (searchString == null) {
            searchString = "";
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final List<ProjectPOJO> projects = projectRepository.searchByString(userId, searchString);
        entityManager.close();
        return projects;
    }

}
