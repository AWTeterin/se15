package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.api.context.IServiceLocator;

public abstract class AbstractService {

    @NotNull
    protected final IServiceLocator serviceLocator;

    public AbstractService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        this.serviceLocator = serviceLocator;
    }

}
