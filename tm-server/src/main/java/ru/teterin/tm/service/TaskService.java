package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IProjectRepository;
import ru.teterin.tm.api.repository.ITaskRepository;
import ru.teterin.tm.api.service.ITaskService;
import ru.teterin.tm.constant.Constant;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.ProjectPOJO;
import ru.teterin.tm.entity.TaskPOJO;
import ru.teterin.tm.exception.AccessForbiddenException;
import ru.teterin.tm.exception.ObjectNotFoundException;
import ru.teterin.tm.repository.ProjectRepository;
import ru.teterin.tm.repository.TaskRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public final class TaskService extends AbstractService implements ITaskService {

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public TaskService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.entityManagerFactory = serviceLocator.getEntityManagerFactory();
    }

    @NotNull
    @Override
    public TaskPOJO findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final TaskPOJO task = taskRepository.findOne(userId, id);
        entityManager.close();
        if (task == null) {
            throw new ObjectNotFoundException();
        }
        return task;
    }

    @NotNull
    @Override
    public List<TaskPOJO> findAll() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final List<TaskPOJO> tasks = taskRepository.findAll();
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskPOJO> findAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final List<TaskPOJO> tasks = taskRepository.findAll(userId);
        entityManager.close();
        return tasks;
    }

    @Override
    public void persist(
        @Nullable final String userId,
        @Nullable final TaskPOJO task
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String taskUserId = task.getUser().getId();
        if (!userId.equals(taskUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.persist(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(
        @Nullable final String userId,
        @Nullable final TaskPOJO task
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (task == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String name = task.getName();
        if (name == null || name.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String taskUserId = task.getUser().getId();
        if (!userId.equals(taskUserId)) {
            throw new AccessForbiddenException();
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.merge(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.remove(userId, id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll(
        @Nullable final String userId
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        taskRepository.removeAll(userId);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @NotNull
    @Override
    public TaskPOJO linkTask(
        @Nullable final String userId,
        @Nullable final String projectId,
        @Nullable final String id
    ) throws Exception {
        final boolean noProjectId = projectId == null || projectId.isEmpty();
        final boolean noTaskId = id == null || id.isEmpty();
        final boolean noUserId = userId == null || userId.isEmpty();
        if (noProjectId || noTaskId || noUserId) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @Nullable final ProjectPOJO project = projectRepository.findOne(userId, projectId);
        if (project == null) {
            entityManager.close();
            throw new ObjectNotFoundException();
        }
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @Nullable final TaskPOJO task = taskRepository.findOne(userId, id);
        if (task == null) {
            entityManager.close();
            throw new ObjectNotFoundException();
        }
        task.setProject(project);
        entityManager.close();
        merge(userId, task);
        return task;
    }

    @NotNull
    @Override
    public List<TaskPOJO> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (sortOption == null) {
            sortOption = Constant.CREATE_ASC_SORT;
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskPOJO> tasks;
        switch (sortOption) {
            default:
                tasks = taskRepository.sortByDateCreate(userId, Constant.ASC);
                break;
            case Constant.CREATE_DESC_SORT:
                tasks = taskRepository.sortByDateCreate(userId, Constant.DESC);
                break;
            case Constant.START_DATE_ASC_SORT:
                tasks = taskRepository.sortByDateStart(userId, Constant.ASC);
                break;
            case Constant.START_DATE_DESC_SORT:
                tasks = taskRepository.sortByDateStart(userId, Constant.DESC);
                break;
            case Constant.END_DATE_ASC_SORT:
                tasks = taskRepository.sortByDateEnd(userId, Constant.ASC);
                break;
            case Constant.END_DATE_DESC_SORT:
                tasks = taskRepository.sortByDateEnd(userId, Constant.DESC);
                break;
            case Constant.STATUS_ASC_SORT:
                tasks = taskRepository.sortByStatus(userId, Constant.ASC);
                break;
            case Constant.STATUS_DESC_SORT:
                tasks = taskRepository.sortByStatus(userId, Constant.DESC);
                break;
        }
        entityManager.close();
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskPOJO> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception {
        if (userId == null || userId.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        if (searchString == null) {
            searchString = "";
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final ITaskRepository taskRepository = new TaskRepository(entityManager);
        @NotNull final List<TaskPOJO> tasks = taskRepository.searchByString(userId, searchString);
        entityManager.close();
        return tasks;
    }

}
