package ru.teterin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.repository.IUserRepository;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.constant.ExceptionConstant;
import ru.teterin.tm.entity.UserPOJO;
import ru.teterin.tm.repository.UserRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.List;

public final class UserService extends AbstractService implements IUserService {

    @NotNull
    private final EntityManagerFactory entityManagerFactory;

    public UserService(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.entityManagerFactory = serviceLocator.getEntityManagerFactory();
    }

    @NotNull
    @Override
    public UserPOJO findOne(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final UserPOJO userPOJO = userRepository.findOne(id);
        entityManager.close();
        if (userPOJO == null) {
            throw new Exception(ExceptionConstant.OBJECT_NOT_FOUND);
        }
        return userPOJO;
    }

    @NotNull
    @Override
    public List<UserPOJO> findAll() throws Exception {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final List<UserPOJO> users = userRepository.findAll();
        entityManager.close();
        return users;
    }

    @Override
    public void persist(
        @Nullable final UserPOJO user
    ) throws Exception {
        if (user == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final String password = user.getPassword();
        if (password == null || password.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.persist(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void merge(
        @Nullable UserPOJO user
    ) throws Exception {
        if (user == null) {
            throw new Exception(ExceptionConstant.EMPTY_OBJECT);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.merge(user);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void remove(
        @Nullable final String id
    ) throws Exception {
        if (id == null || id.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_ID);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.remove(id);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        userRepository.removeAll();
        entityManager.getTransaction().commit();
        entityManager.close();
    }

    @NotNull
    @Override
    public UserPOJO findByLogin(
        @Nullable final String login
    ) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_LOGIN);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        @Nullable final UserPOJO user = userRepository.findByLogin(login);
        entityManager.close();
        if (user == null) {
            throw new Exception(ExceptionConstant.OBJECT_NOT_FOUND);
        }
        return user;
    }

    @Override
    public boolean loginIsFree(
        @Nullable final String login
    ) throws Exception {
        if (login == null || login.isEmpty()) {
            throw new Exception(ExceptionConstant.EMPTY_LOGIN);
        }
        @NotNull final EntityManager entityManager = entityManagerFactory.createEntityManager();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        return userRepository.loginIsFree(login);
    }

}
