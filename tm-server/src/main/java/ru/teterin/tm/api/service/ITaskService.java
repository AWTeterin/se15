package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.TaskPOJO;

import java.util.List;

public interface ITaskService {

    @NotNull
    public TaskPOJO findOne(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception;

    @NotNull
    public List<TaskPOJO> findAll();

    @NotNull
    public List<TaskPOJO> findAll(
        @Nullable final String userId
    ) throws Exception;

    public void persist(
        @Nullable final String userId,
        @Nullable final TaskPOJO task
    ) throws Exception;

    public void merge(
        @Nullable final String userId,
        @Nullable final TaskPOJO task
    ) throws Exception;

    public void remove(
        @Nullable final String userId,
        @Nullable final String id
    ) throws Exception;

    public void removeAll();

    public void removeAll(
        @Nullable final String userId
    ) throws Exception;

    @NotNull
    public TaskPOJO linkTask(
        @Nullable final String userId,
        @Nullable final String projectId,
        @Nullable final String id
    ) throws Exception;

    @NotNull
    public List<TaskPOJO> findAndSortAll(
        @Nullable final String userId,
        @Nullable String sortOption
    ) throws Exception;

    @NotNull
    public List<TaskPOJO> searchByString(
        @Nullable final String userId,
        @Nullable String searchString
    ) throws Exception;

}
