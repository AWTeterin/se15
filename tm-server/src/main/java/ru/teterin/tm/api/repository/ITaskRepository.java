package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.TaskPOJO;

import java.util.List;

public interface ITaskRepository {

    @Nullable
    public TaskPOJO findOne(
        @NotNull final String userId,
        @NotNull final String id
    );

    @NotNull
    public List<TaskPOJO> findAll();

    @NotNull
    public List<TaskPOJO> findAll(
        @NotNull final String userId
    );

    public void persist(
        @NotNull final TaskPOJO task
    );

    public void merge(
        @NotNull final TaskPOJO task
    );

    public void remove(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception;

    public void removeAll();

    public void removeAll(
        @NotNull final String userId
    );

    @NotNull
    public List<TaskPOJO> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    );

    @NotNull
    public List<TaskPOJO> sortByDateCreate(
        @NotNull final String userId,
        @NotNull final String sortType
    );

    @NotNull
    public List<TaskPOJO> sortByDateStart(
        @NotNull final String userId,
        @NotNull final String sortType
    );

    @NotNull
    public List<TaskPOJO> sortByDateEnd(
        @NotNull final String userId,
        @NotNull final String sortType
    );

    @NotNull
    public List<TaskPOJO> sortByStatus(
        @NotNull final String userId,
        @NotNull final String sortType
    );

    @NotNull
    public List<TaskPOJO> findAllByProjectId(
        @NotNull final String userId,
        @NotNull final String projectId
    );

}