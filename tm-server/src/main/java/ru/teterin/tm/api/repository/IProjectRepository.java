package ru.teterin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.ProjectPOJO;

import java.util.List;

public interface IProjectRepository {

    @Nullable
    public ProjectPOJO findOne(
        @NotNull final String userId,
        @NotNull final String id
    );

    @NotNull
    public List<ProjectPOJO> findAll();

    @NotNull
    public List<ProjectPOJO> findAll(
        @NotNull final String userId
    );

    public void persist(
        @NotNull final ProjectPOJO task
    );

    public void merge(
        @NotNull final ProjectPOJO task
    );

    public void remove(
        @NotNull final String userId,
        @NotNull final String id
    ) throws Exception;

    public void removeAll();

    public void removeAll(
        @NotNull final String userId
    );

    @NotNull
    public List<ProjectPOJO> searchByString(
        @NotNull final String userId,
        @NotNull final String searchString
    );

    @NotNull
    public List<ProjectPOJO> sortByDateCreate(
        @NotNull final String userId,
        @NotNull final String sortType
    );

    @NotNull
    public List<ProjectPOJO> sortByDateStart(
        @NotNull final String userId,
        @NotNull final String sortType
    );

    @NotNull
    public List<ProjectPOJO> sortByDateEnd(
        @NotNull final String userId,
        @NotNull final String sortType
    );

    @NotNull
    public List<ProjectPOJO> sortByStatus(
        @NotNull final String userId,
        @NotNull final String sortType
    );

}
