package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.enumerated.Role;

public interface ISessionService {

    public void validate(
        @Nullable final String session
    ) throws Exception;

    public void validateWithRole(
        @Nullable final String session,
        @Nullable final Role role
    ) throws Exception;

    public void close(
        @Nullable final String session
    ) throws Exception;

    @Nullable
    public String getToken(
        @Nullable final String login,
        @Nullable final String password
    ) throws Exception;

    @NotNull
    public String getUserId(
        @Nullable final String session
    ) throws Exception;

}
