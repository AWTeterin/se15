package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    public String getServerHost();

    @NotNull
    public String getServerPort();

    @NotNull
    public String getSessionSalt();

    @NotNull
    public Integer getSessionCycle();

    @NotNull
    public String getJdbcDriver();

    @NotNull
    public String getJdbcUrl();

    @NotNull
    public String getJdbcLogin();

    @NotNull
    public String getJdbcPassword();

    @NotNull
    public String getSecretKey();

}
