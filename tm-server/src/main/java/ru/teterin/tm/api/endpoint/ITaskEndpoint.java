package ru.teterin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.dto.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ITaskEndpoint {

    @WebMethod
    public void mergeTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "task") @Nullable final Task task
    ) throws Exception;

    @WebMethod
    public void persistTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "task") @Nullable final Task task
    ) throws Exception;

    @NotNull
    @WebMethod
    public Task findOneTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception;

    @NotNull
    @WebMethod
    public List<Task> findAllTask(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception;

    @WebMethod
    public void removeTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception;

    @WebMethod
    public void removeAllTasks(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception;

    @NotNull
    @WebMethod
    public List<Task> searchTaskByString(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "string") @Nullable final String string
    ) throws Exception;

    @NotNull
    @WebMethod
    public List<Task> sortAllTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "sortOption") @Nullable final String sortOption
    ) throws Exception;

    @WebMethod
    public void linkTask(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "projectId") @Nullable final String projectId,
        @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception;

}
