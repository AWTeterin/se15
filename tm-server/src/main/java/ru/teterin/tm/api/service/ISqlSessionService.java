package ru.teterin.tm.api.service;

import org.apache.ibatis.session.SqlSessionFactory;

public interface ISqlSessionService {

    public SqlSessionFactory getSqlSessionFactory();

}
