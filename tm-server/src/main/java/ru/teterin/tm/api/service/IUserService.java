package ru.teterin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.entity.UserPOJO;

import java.util.List;

public interface IUserService {

    @NotNull
    public UserPOJO findOne(
        @Nullable final String id
    ) throws Exception;

    @NotNull
    public List<UserPOJO> findAll() throws Exception;

    public void remove(
        @Nullable final String id
    ) throws Exception;

    public void removeAll() throws Exception;

    public void persist(
        @Nullable final UserPOJO user
    ) throws Exception;

    public void merge(
        @Nullable final UserPOJO user
    ) throws Exception;

    @NotNull
    public UserPOJO findByLogin(
        @Nullable final String login
    ) throws Exception;

    public boolean loginIsFree(
        @Nullable final String login
    ) throws Exception;

}
