package ru.teterin.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.dto.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint {

    @WebMethod
    public void mergeUser(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "user") @Nullable final User user
    ) throws Exception;

    @WebMethod
    public void persistUser(
        @WebParam(name = "user") @Nullable final User user
    ) throws Exception;

    @NotNull
    @WebMethod
    public User findOneUser(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "userId") @Nullable final String userId
    ) throws Exception;

    @WebMethod
    public void removeUser(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "userId") @Nullable final String userId
    ) throws Exception;

    @WebMethod
    public boolean loginIsFree(
        @WebParam(name = "login") @Nullable final String login
    ) throws Exception;

    @WebMethod
    public String getUserId(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "login") @Nullable final String login
    ) throws Exception;

    @WebMethod
    public void setLogin(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "login") @Nullable final String login
    ) throws Exception;

    @WebMethod
    public void setPassword(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "oldPassword") @Nullable final String oldPassword,
        @WebParam(name = "newPassword") @Nullable final String newPassword
    ) throws Exception;

}
