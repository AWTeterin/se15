package ru.teterin.tm.api.endpoint;

import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @Nullable
    @WebMethod
    public String openSession(
        @WebParam(name = "login") @Nullable final String login,
        @WebParam(name = "password") @Nullable final String password
    ) throws Exception;

    @Nullable
    @WebMethod
    public String closeSession(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception;

}
