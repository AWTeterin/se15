package ru.teterin.tm.enumerated;

import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

@AllArgsConstructor
public enum Role {

    USER("USER"),
    ADMIN("ADMIN");

    @NotNull
    private final String name;

    @NotNull
    public String displayName() {
        return name;
    }

}
