package ru.teterin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.dto.Project;
import ru.teterin.tm.enumerated.Status;
import org.hibernate.annotations.Cache;
import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
@Cacheable
@Table(name = "app_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class ProjectPOJO extends AbstractPOJO {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "userId")
    private UserPOJO user;

    @NotNull
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private List<TaskPOJO> tasks;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateCreate = new Date();

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateEnd = new Date();

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    public Project toProject() {
        @NotNull final Project project = new Project();
        project.setId(id);
        project.setUserId(user.getId());
        project.setName(name);
        project.setDescription(description);
        project.setDateCreate(dateCreate);
        project.setDateStart(dateStart);
        project.setDateEnd(dateEnd);
        project.setStatus(status);
        return project;
    }

}
