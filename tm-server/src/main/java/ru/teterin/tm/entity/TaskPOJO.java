package ru.teterin.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import ru.teterin.tm.dto.Task;
import ru.teterin.tm.enumerated.Status;
import org.hibernate.annotations.Cache;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@Cacheable
@Table(name = "app_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class TaskPOJO extends AbstractPOJO {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "userId")
    private UserPOJO user;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "projectId")
    private ProjectPOJO project;

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private Date dateCreate = new Date();

    @NotNull
    private Date dateStart = new Date();

    @NotNull
    private Date dateEnd = new Date();

    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @NotNull
    public Task toTask() {
        @NotNull final Task task = new Task();
        task.setId(id);
        task.setUserId(user.getId());
        task.setProjectId(project.getId());
        task.setName(name);
        task.setDescription(description);
        task.setDateCreate(dateCreate);
        task.setDateStart(dateStart);
        task.setDateEnd(dateEnd);
        task.setStatus(status);
        return task;
    }

}
