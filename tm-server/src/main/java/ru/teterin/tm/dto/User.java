package ru.teterin.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.entity.ProjectPOJO;
import ru.teterin.tm.entity.TaskPOJO;
import ru.teterin.tm.entity.UserPOJO;
import ru.teterin.tm.enumerated.Role;

import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public final class User extends AbstractDTO implements Serializable {

    public static final long serialVersionUID = 3;

    @NotNull
    protected String login = "";

    @NotNull
    private String password = "";

    @NotNull
    private Role role = Role.USER;

    @NotNull
    public UserPOJO toUserPOJO(
        @NotNull final IServiceLocator serviceLocator
    ) throws Exception {
        @NotNull final UserPOJO userPOJO = new UserPOJO();
        @Nullable final List<ProjectPOJO> projects = serviceLocator.getProjectService()
            .findAll(id);
        @NotNull final List<TaskPOJO> tasks = serviceLocator.getTaskService()
            .findAll(id);
        userPOJO.setId(id);
        userPOJO.setLogin(login);
        userPOJO.setPassword(password);
        userPOJO.setRole(role);
        userPOJO.setProjects(projects);
        userPOJO.setTasks(tasks);
        return userPOJO;
    }

}
