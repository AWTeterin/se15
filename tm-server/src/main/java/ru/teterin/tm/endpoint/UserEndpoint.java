package ru.teterin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.IUserEndpoint;
import ru.teterin.tm.api.service.IUserService;
import ru.teterin.tm.dto.User;
import ru.teterin.tm.entity.UserPOJO;
import ru.teterin.tm.exception.AccessForbiddenException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.teterin.tm.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @NotNull
    private final IUserService userService;

    public UserEndpoint(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.userService = serviceLocator.getUserService();
    }

    @Override
    @WebMethod
    public void mergeUser(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "user") @Nullable final User user
    ) throws Exception {
        validateSession(session);
        userService.merge(user.toUserPOJO(serviceLocator));
    }

    @Override
    @WebMethod
    public void persistUser(
        @WebParam(name = "user") @Nullable final User user
    ) throws Exception {
        userService.persist(user.toUserPOJO(serviceLocator));
    }

    @NotNull
    @Override
    @WebMethod
    public User findOneUser(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        @Nullable final User user = userService.findOne(userId).toUser();
        return user;
    }

    @Override
    @WebMethod
    public void removeUser(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "userId") @Nullable final String userId
    ) throws Exception {
        validateSession(session);
        userService.remove(userId);
    }

    @Override
    @WebMethod
    public boolean loginIsFree(
        @WebParam(name = "login") @Nullable final String login
    ) throws Exception {
        return userService.loginIsFree(login);
    }

    @WebMethod
    public String getUserId(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "login") @Nullable final String login
    ) throws Exception {
        validateSession(session);
        @NotNull final UserPOJO user = userService.findByLogin(login);
        return user.getId();
    }

    @WebMethod
    public void setLogin(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "login") @Nullable final String login
    ) throws Exception {
        validateSession(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final UserPOJO user = userService.findOne(userId);
        user.setLogin(login);
        userService.merge(user);
    }

    @WebMethod
    public void setPassword(
        @WebParam(name = "session") @Nullable final String session,
        @WebParam(name = "oldPassword") @Nullable final String oldPassword,
        @WebParam(name = "newPassword") @Nullable final String newPassword
    ) throws Exception {
        validateSession(session);
        @NotNull final String userId = serviceLocator.getSessionService().getUserId(session);
        @NotNull final UserPOJO user = userService.findOne(userId);
        if (oldPassword == null || oldPassword.isEmpty() || newPassword == null || newPassword.isEmpty()) {
            throw new AccessForbiddenException();
        }
        if (!oldPassword.equals(user.getPassword())) {
            throw new AccessForbiddenException();
        }
        user.setLogin(newPassword);
        userService.merge(user);
    }

}
