package ru.teterin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.teterin.tm.api.context.IServiceLocator;
import ru.teterin.tm.api.endpoint.ISessionEndpoint;
import ru.teterin.tm.api.service.ISessionService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.teterin.tm.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    private final ISessionService sessionService;

    public SessionEndpoint(
        @NotNull final IServiceLocator serviceLocator
    ) {
        super(serviceLocator);
        this.sessionService = serviceLocator.getSessionService();
    }

    @Nullable
    @Override
    @WebMethod
    public String openSession(
        @WebParam(name = "login") @Nullable final String login,
        @WebParam(name = "password") @Nullable final String password
    ) throws Exception {
        @Nullable final String token = sessionService.getToken(login, password);
        return token;
    }

    @Nullable
    @Override
    @WebMethod
    public String closeSession(
        @WebParam(name = "session") @Nullable final String session
    ) throws Exception {
        sessionService.close(session);
        return session;
    }

}
